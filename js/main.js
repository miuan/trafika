angular.module('ngView', [], function($routeProvider, $locationProvider) {
    $routeProvider.when('/Book/:bookId', {
        templateUrl: 'book.html',
        controller: BookCntl
    });
    $routeProvider.when('/Book/:bookId/ch/:chapterId', {
        templateUrl: 'chapter.html',
        controller: ChapterCntl
    });

    // configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode(true);
});

function MainCntl($scope, $http, $route, $routeParams, $location) {
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;

    $scope.method = 'login';
    $scope.api = 'user';
    $scope.exturl = 'nazdar';
    $scope.host = 'http://localhost:5000/api/'

    $scope.session;
    $scope.client = 'test';
    $scope.cversion = '1.0';

    $scope.client_enabled = true;
    $scope.cversion_enabled = true;

    $scope.curl = 'curl.exe'

    $scope.sessionVisible = true;
    $scope.sessionDisabled = false;

    $scope.params = [];
    $scope.posts = [];

    $scope.result;
    $scope.resultHeader;
    $scope.resultStatus;


    $scope.userConfig = {
        'default':{
            api:'user',
            session: false,
            param: {email:'karel@sezam.cz'},
            post : []
        },
        'login':{
            session:true,
            post: {'pass': 'password'}
        },
        'create':{
            param: {
                email:'karel@sezam.cz',
                name:'karel',
                surname:'zeman',
                driver_licence:'x2032x'},
            post: {'pass': 'password'}
            },
        exists:{}};

    $scope.changeAPI = function(method){
        //alert(api);


        act = _.clone($scope.userConfig['default']);

        if($scope.userConfig.hasOwnProperty(method)){
            // fill settig to default
            nov = _.defaults($scope.userConfig[method], act);
            nov.param = _.defaults($scope.userConfig[method].param, act.param);
//            _.keys(nov).forEach(function(element, index){
//
//                act[element] = nov[element];
//            });
            act = nov;
        }

        //alert(act.api);
        $scope.api = act.api;
        $scope.method = method;

        $scope.showSession(act.session);

        showParams(act.param);
        showPosts(act.post);
        $scope.paramChange();
    }



    $scope.showSession = function(show){
        $scope.sessionVisible = show;
        $scope.sessionDisabled = !show;
    }

    showPosts = function(posts){
        newposts = []
        post = _.pairs(posts);
        post.forEach(function(element, index){
            o = {
                name : element[0],
                value : element[1]
            }

            newposts.push(o);
        });

        $scope.posts = newposts;

    }

    showParams = function(params){
        param = [];
        params = _.pairs(params);
        params.forEach(function(element, index){
            o = {
                name : element[0],
                value : element[1]
            }

            param.push(o);
        });

        $scope.params = param;
        showUrl(param);

    }

    showUrl = function(params){
        exturl = '';
        params.forEach(function(element, index){
            if(element.value.length > 0) {
                exturl += '/' + element.value;
            }
        });

        $scope.exturl = exturl;
    }

    showCURLBase = function(posts){
        postdata = '';
        ar = [];


        if(!$scope.sessionDisabled && _.isString($scope.session) && $scope.session.length > 0){
            ar.push('session=' + $scope.session);
        }

        if(!$scope.client_enable){
            ar.push('client=' + $scope.client);
        }

        if(!$scope.cversion_enable){
            ar.push('cversion=' + $scope.cversion);
        }

        posts.forEach(function(el,idndex){
            //pair = _.pairs(el);
            if(isNaN(el.value)){
                ar.push(el.name + '=' + el.value);
            }
        });

        //alert(ar.length);
        if(ar.length > 0){
            ar.forEach(function(el, index){
                postdata += el + '&';
            });
            postdata = postdata.substr(0, postdata.length -1);
        }

        return postdata;
    }


    showCURL = function(param, posts){
        curl = 'curl -i';

        postdata = showCURLBase(posts);

        if(postdata.length > 0){
            curl += ' --data "' + postdata + '"';
        }

        curl += ' ' + $scope.host + $scope.api + '/' + $scope.method + '/' + $scope.exturl;


        $scope.curl=curl;
    }

    $scope.paramChange = function(){
        showUrl($scope.params);
        showCURL($scope.params, $scope.posts);
    }


    $scope.call = function(){
        fullurl = $scope.host + $scope.api + '/' + $scope.method + $scope.exturl;
        //$httpProvider.defaults.headers.get['My-Header']='value'
        //alert(fullurl);
        // showCURLBase($scope.posts)
        console.log('ahojwe');

        var data = {};
        $scope.posts.forEach(function(el, inx){
            data[el.name] = el.value;
            //console.log(data[el.name]);
        });
        data.client = $scope.client;
        data.cversion = $scope.cversion;
        if(!$scope.sessionDisabled){
            data.session = $scope.session;
        }

        $scope.result = '';
        $scope.resultHeader = '';
        $scope.resultStatus = 'send..';


        $http
            .post(fullurl, data)
            .success(function(data, status, header, config){
                //alert(data);
                $scope.result = json2html(data);
                $scope.resultHeader = data;
                $scope.resultStatus = status + ' OK';
            })
            .error(function(data, status, header, config){
                //alert(data);
                $scope.result = data;
                $scope.resultHeader = header;
                $scope.resultStatus = status + ' ERROR';
        });
    }

    $scope.changeAPI('login');
    $scope.call();
}

function BookCntl($scope, $routeParams) {
    $scope.name = "BookCntl";
    $scope.params = $routeParams;
}

function ChapterCntl($scope, $routeParams) {
    $scope.name = "ChapterCntl";
    $scope.params = $routeParams;
}
