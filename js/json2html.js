function json2html(json){

    return formatObject(json);
}

function cell(value, cls){
    return '<span class="'+cls+'">'+value+'</span>';
}

function block(txt, cls){
    return '<div class="'+cls+'">'+txt+'</div>';
}


function formatValue(obj, key){
    var val = obj[key];
    var valcls = 'json2html_val';
    if(_.isNumber(val)){
       valcls += '_number'
    } else if(_.isString(val)){
        valcls += '_text'
        val = '\'' + val +'\'';
    }

   return cell(key, 'json2html_key') + ' : ' + cell(val, valcls);
}

function formatObject(obj){
    var text = '{';
    var keys = _.keys(obj);


    var last = _.last(keys);
    keys.forEach(function(key, idx){
        var val = '';
        var element = obj[key];


        if(_.isObject(element)){
          val = formatObject(element);
          console.log('Object:' + key);
        } else {
          val = formatValue(obj, key);
           // console.log('isObject val : ' + key);
        }

        if(last != key){
            val += ',';
        }

        text += block(val, 'json2html_block');
        //console.log(blk);
        //out += val;
        //out += deep(blk, dp+1);

    });

    text += '}';

    return text;
}


